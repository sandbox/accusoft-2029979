-- INTRODUCTION --

   Prizm Cloud is a document viewer that enables you to display hundreds of 
   different kinds of files on your website without worrying about whether your
   visitors have the software to view them and without installing any additional 
   hardware or software. The document files stay on your server, so you can 
   update, edit and change them anytime. Prizm Cloud supports more than 300 file
   types, including DOC, PDF, PPT, XLS and CAD.

-- INSTALLATION --

1. Download and unpack the module.
2. Place the module in your modules folder ("sites/all/modules/").
3. Enable the module under admin/modules

-- USAGE --

After installation,
  configure at "admin/config/user-interface/prizmcloud" 
  or "admin/prizmcloud/config".

Options :-
License Key    : This is the license key provided by Accusoft.  Sign up for 
				 your free key at http://prizmcloud.accusoft.com/register.html

Document URL   : The URL of the document you wish to display in the viewer.

Viewer Type    : Choose from HTML5 and Flash.
				 (Default: Flash)

Viewer Width   : The width of the viewer in pixels.
				 (Default: 600)

Viewer Height  : The height of the viewer in pixels.
				 (Default: 800)

Print Button   : The ability to switch save document button on and off
                 (Values: Yes/No) (Default: Yes)

Toolbar Color  : Enter a hexadecimal value without the hash tag (#).

For more documentation, details, and tips and tricks, 
visit http://prizmcloud.accusoft.com/.
